#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

typedef struct Cell Cell;
typedef struct PilhaE PilhaE;

struct Cell
{
    char item;
    Cell *next;
};


struct PilhaE
{
    Cell *topo;
};

Cell* criar_celula(char key)
{
    Cell *c = (Cell*) malloc(sizeof(Cell));

    c->item = key;

    c->next = NULL;

    return c;
}


PilhaE* criar_pilhaE()
{
    PilhaE* p = (PilhaE*) malloc(sizeof(PilhaE));

    p->topo = NULL;

    return p;
}


int pilhaE_vazia(PilhaE *p)
{
    return (p == NULL) || (p->topo == NULL);
}


void empilhar(char key, PilhaE *p)
{
    Cell *aux;
    if (p == NULL)
        p = criar_pilhaE();

    aux = criar_celula(key);

    aux->next = p->topo;

    p->topo = aux;
}


char desempilhar(PilhaE *p)
{
    Cell *aux;
    char item='\0';

    if (!pilhaE_vazia(p))
    {
        aux = p->topo;
        item = aux->item;
        p->topo = aux->next;
        free(aux);
    }

    return item;
}

void imprimir_pilha(PilhaE *p)
{
    Cell *aux;
    if (!pilhaE_vazia(p))
    {
        aux = p->topo;

        while (aux != NULL)
        {
            printf("%c\n", aux->item);

            aux = aux->next;
        }
    }
}


int liberar_pilha(PilhaE *p)
{
    if (p != NULL)
    {
        while (!pilhaE_vazia(p))
            desempilhar(p);

        free(p);

        return 1;
    }

    return 0;
}

int verify_p(char p[])
{
    PilhaE *aux;
    aux=criar_pilhaE();
    for(int j=0; !(p[j]== '\0'); j++)
    {
        if(p[j]=='(')
        {
            empilhar(p[j], aux);
        }
        else if(p[j]==')'&& !(pilhaE_vazia(aux)))
        {
            desempilhar(aux);
        }
        else if(p[j]==')'&& pilhaE_vazia(aux)){
            liberar_pilha(aux);
            return 0;
        }
    }

    if((pilhaE_vazia(aux))){
        liberar_pilha(aux);
        return 1;
    }
    return 0;
}


int main()
{
    int n;
    scanf("%d", &n);
    char palavra[n][100];
    for(int i=0; i<n; i++)
    {
        scanf("%s",palavra[i]);
    }
    for(int i=0; i<n; i++)
    {
        if(verify_p(palavra[i]))
            printf("correct\n");
        else
            printf("incorrect\n");
    }
    return 0;
}


