#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
void frac(float n, int * in, float * fr){
    *in = (int)n;
    *fr = n - *in;
}
int main() {
    int n, inte;
    float aux,fra;
    scanf("%d", &n);
    
    for(int i=0; i <n; i++){
        scanf("%f",&aux);
        frac(aux, &inte, &fra);   
        printf("N=%.3f I=%d F=%.3f\n", aux, inte, fra);
    }
}

