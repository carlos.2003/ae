#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

typedef struct
{
    int i; // parte inteira
    int f; // parte fracionária
} Real;

Real *criar(int i, int f)
{
    Real *x = malloc(sizeof(Real));
    x->i = i;
    x->f = f;

    return x;
}

// Soma entre dois números reais
Real *soma(Real *r1, Real *r2)
{
    int x, y;
    x = r1->i + r2->i;
    if (r1->f + r2->f > 100)
    {
        y = (r1->f + r2->f) - 100;
        x++;
    }
    else
    {
        y = (r1->f + r2->f);
    }
    Real *sm = criar(x, y);
    return sm;
}

// Subtração entre dois números reais
Real *subtracao(Real *r1, Real *r2)
{
    int x, y;
    x = (r1->i - r2->i);

    y = abs(r1->f - r2->f);

    Real *resto = criar(x, y);
    return resto;
}
// Arredondar
int arredondar(Real *r1)
{
    int ar, x = r1->i;
    if (r1->f >= 50)
    {
        x++;
    }
    return x;
}

void imprimir(Real *r1)
{
    float num;
    num = (float)r1->i + ((float)r1->f / 100);

    printf("%.2f\n", num);
}
int main(void)
{
    Real *num1, *num2;
    int x, y;

    scanf("%d %d", &x, &y);
    num1 = criar(x, y);
    scanf("%d %d", &x, &y);
    num2 = criar(x, y);
    imprimir(num1);
    imprimir(num2);
    imprimir(soma(num1, num2));
    imprimir(subtracao(num1, num2));
    printf("%d %d", arredondar(num1), arredondar(num2));
}
