#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>


typedef struct
{
    int numerador;
    int denominador;
}rac;


rac resultado(rac a, rac b){
    rac res;
    res.numerador=a.numerador*b.denominador;
    res.denominador=a.denominador*b.numerador;

    return res;
}
int main(void)
{
    rac a;
    rac res;
    scanf("%d  %d" , &a.numerador, &a.denominador);
    rac b;
    scanf("%d  %d" , &b.numerador, &b.denominador);
    res=resultado(a,b);
    printf("%d %d", res.numerador, res.denominador);
}

