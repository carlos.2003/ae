#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
typedef struct
{
    int *v; // Array (vetor) de tamanho n
    int n;  // tamanho do v
    int q;  // quantidade de elementos preenchidos no vetor
} Vetor;

Vetor *iniciar(int n)
{
    Vetor *vet = malloc(sizeof(Vetor));
    vet->v = malloc(n * sizeof(int));
    vet->n = n;
    vet->q = 0;
    return vet;
}

int inserir(Vetor *v, int x)
{
    if (v->q < v->n)
    {
        v->v[v->q] = x;
        v->q++;
        return 1;
    }
    return 0;
} // inserir x na última posição. A função retorna 1 se a operação foi bem-sucedida

Vetor *concatenar(Vetor *v1, Vetor *v2)
{
    int i;
    Vetor *v3 = iniciar(v1->q + v2->q);

    for (i = 0; i < v1->q; i++)
        inserir(v3, v1->v[i]);

    for (i = 0; i < v2->q; i++)
        inserir(v3, v2->v[i]);

    return v3;
}

Vetor *soma(Vetor *v1, Vetor *v2)
{
    int i;
    Vetor *v3 = iniciar(v1->q > v2->q ? v1->q : v2->q);

    for (i = 0; (i < v1->q) && (i < v2->q); i++)
        inserir(v3, v1->v[i] + v2->v[i]);

    for (i = v2->q; i < v1->q; i++)
        inserir(v3, v1->v[i]);

    for (i = v1->q; i < v2->q; i++)
        inserir(v3, v2->v[i]);

    return v3;
}

int main(void)
{
    Vetor *vt1, *vt2, *vt3, *sm;
    int n, aux;
    scanf("%d", &n);
    
    vt1 = iniciar(n);
    for (int i = 0; i < n-1; i++)
    {
        scanf("%d", &aux);
        inserir(vt1, aux);
    }
    scanf("%d", &n);
    
    vt2 = iniciar(n);
    for (int i = 0; i < n-1; i++)
    {
        scanf("%d", &aux);
        inserir(vt2, aux);
    }

    vt3 = iniciar(vt1->q + vt2->q);
    vt3 = concatenar(vt1, vt2);

    for (int i = 0; i < vt3->q; i++)
    {
        printf("%d ", vt3->v[i]);
    }
    printf("\n");


    sm = iniciar(vt1->q > vt2->q ? vt1->q : vt2->q);
    sm = soma(vt1, vt2);
    for (int i = 0; i < sm->q; i++)
    {
        printf("%d ", sm->v[i]);
    }
}

