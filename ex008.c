    #include <stdio.h>
    #include <string.h>
    #include <math.h>
    #include <stdlib.h>

    char *descodifica(char *msg1, int *nums, int tamanho)
    {
        char *frase = malloc(tamanho * sizeof(int));
        for (int i = 0; i < tamanho; i++)
        {
            frase[i] = msg1[nums[i] - 1];
        }
        return frase;
    }

    int main(void)
    {
        char str[255], *mensagem;
        int *cod, aux, tamvet = 0;
        cod = (int *)malloc(100 * sizeof(int));
        scanf("%s", str);
        for (int i = 0; aux != -1; i++)
        {
            scanf("%d", &aux);

            cod[i] = aux;
            tamvet++;
        }
        mensagem = descodifica(str, cod, tamvet - 1);

        printf("%s", mensagem);
    }