#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

typedef struct{
    float real, img, mod;
}complexo;

void atualizar_complexo(complexo *c, float real, float img){
    c->real=real;
    c->img=img;
    c->mod=sqrt(real * real + img * img);
}

int main(void) {
    complexo point;
    float real, img;
    scanf("%f %f", &real, &img);
    atualizar_complexo(&point,real,img);
    printf("%.1f",point.real);
    if(point.img>=0)
        printf("+");
    printf("%.1fi", point.img);
    printf("\n%.1f", point.mod);
}
